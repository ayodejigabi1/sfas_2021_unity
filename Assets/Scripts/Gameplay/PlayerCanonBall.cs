﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCanonBall : MonoBehaviour
{
    private Rigidbody rb;

    [SerializeField]
    private float shootSpeed;

    [SerializeField]
    private float destroyAfterSeconds;

    [SerializeField]
    private float dmgValue;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {
        rb.AddForce(Player.instance.GetComponent<PlayerCamera>().PlayerTop.transform.forward * shootSpeed, ForceMode.Force);
        Destroy(gameObject, destroyAfterSeconds);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Tank AI"))
        {
            EnemyHealth enemyHealth = collision.transform.GetComponent<EnemyHealth>();
            enemyHealth.ReduceHealth(dmgValue);
            Destroy(gameObject);
        }
    }
}
