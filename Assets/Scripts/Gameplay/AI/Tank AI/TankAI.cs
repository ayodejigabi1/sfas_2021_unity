﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TankAI : MonoBehaviour
{
    private float patrolTime;

    private float waitTimeBeforeShooting;

    private bool isWaitingDuringPatrol = false;

    private bool isWaitingBeforeShooting = false;

    [SerializeField] private MeshRenderer top;

    [SerializeField] private MeshRenderer bottom;

    [SerializeField] private MeshRenderer barrel;

    [SerializeField]private Transform[] patrolPoints;

    [SerializeField]private NavMeshAgent agent;

    [SerializeField]private Player player;

    [SerializeField]private float radius;

    [SerializeField] private GameObject canonBall;

    [SerializeField] private Transform spawnTransform;

    public NavMeshAgent Agent => agent;

    private Node tankTopNode;
    public Transform[] PatrolPoints => patrolPoints;
    public Player Player => player;
    public float Radius => radius;
    public MeshRenderer Top => top;
    public MeshRenderer Bottom => bottom;
    public MeshRenderer Barrel => barrel;
    public float PatrolTime { get => patrolTime; set { patrolTime = value; } }
    public float WaitTimeBeforeShooting { get => waitTimeBeforeShooting; set { waitTimeBeforeShooting = value; } }
    public bool IsWaitingDuringPatrol { get => isWaitingDuringPatrol; set { isWaitingDuringPatrol = value; } }
    public bool IsWaitingBeforeShooting { get => isWaitingBeforeShooting; set { isWaitingBeforeShooting = value; } }
    public GameObject CanonBall  => canonBall;
    public Transform SpawnTransform => spawnTransform;


    // Start is called before the first frame update
    void Start()
    {
        ConstructBehaviourTree();
    }

    // Update is called once per frame
    void Update()
    {
        tankTopNode.Evaluate();
        if (tankTopNode.State == NodeState.FAILURE)
        {
            print("Failure");
            agent.isStopped = true;
        }

        if (isWaitingDuringPatrol)
        {
            patrolTime += Time.deltaTime;
        }

        if (isWaitingBeforeShooting)
        {
            waitTimeBeforeShooting += Time.deltaTime;
        }

    }

    private void ConstructBehaviourTree()
    {
        Patrol patrolNode = new Patrol(agent, this);
        Selector searchNode = new Selector(new List<Node> { patrolNode });
        Shoot shootNode = new Shoot(agent, this, player.transform);
        IsPlayerFound isPlayerFoundNode = new IsPlayerFound(radius, this);
        Sequence destroyEnemyNode = new Sequence(new List<Node> { isPlayerFoundNode, shootNode });
        tankTopNode = new Selector(new List<Node> { searchNode, destroyEnemyNode });
    }

    public void SetAlertLevel(Color color)
    {
        top.material.color = color;
        bottom.material.color = color;
        barrel.material.color = color;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
