﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Shoot : Node
{
    private NavMeshAgent agent;
    private TankAI tankAI;
    private Transform target;
    private Vector3 currentVelocity;
    private float smoothDamp;
    private float waitTime;

    public Shoot(NavMeshAgent agent, TankAI tankAI, Transform target)
    {
        this.agent = agent;
        this.tankAI = tankAI;
        this.target = target;
        smoothDamp = 1f;
        waitTime = 2f;
    }

    public override NodeState Evaluate()
    {
        Debug.Log("Shoot");
        agent.isStopped = true;
        tankAI.SetAlertLevel(Color.red);
        Vector3 direction = target.position - tankAI.transform.position;
        Vector3 currentDirection = Vector3.SmoothDamp(tankAI.transform.forward, direction, ref currentVelocity, smoothDamp);
        Quaternion rotation = Quaternion.LookRotation(currentDirection, Vector3.up);
        tankAI.transform.rotation = rotation;
        tankAI.IsWaitingBeforeShooting = true;

        if (tankAI.WaitTimeBeforeShooting > waitTime)
        {
            tankAI.IsWaitingBeforeShooting = false;
            tankAI.WaitTimeBeforeShooting = 0f;
            EnemyCanonBall canonball = Object.Instantiate(tankAI.CanonBall, tankAI.SpawnTransform.position, Quaternion.identity).GetComponent<EnemyCanonBall>();
            canonball.TankAI = tankAI;
        }
        
        state = NodeState.RUNNING;
        return state;
    }
}
