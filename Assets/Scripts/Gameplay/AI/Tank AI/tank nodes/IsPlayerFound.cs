﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsPlayerFound : Node
{
    private float radius;
    private TankAI tankAI;

    public IsPlayerFound(float radius, TankAI tankAI)
    {
        this.radius = radius;
        this.tankAI = tankAI;
    }

    public override NodeState Evaluate()
    {
        Debug.Log("IsPlayerFound");
        float distance = Vector3.Distance(tankAI.Player.transform.position, tankAI.transform.position);

        if (distance <= radius)
        {
            tankAI.SetAlertLevel(Color.red);
            state = NodeState.SUCCESS;
        }
        else
        {
            state = NodeState.FAILURE;
        }

        return state;
    }
}
