﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Patrol : Node
{
    private NavMeshAgent agent;
    private TankAI tankAI;
    private int index;
    private float maxWaitTime;
    private Transform[] patrolPoint;

    public Patrol(NavMeshAgent agent, TankAI tankAI)
    {
        this.agent = agent;
        this.tankAI = tankAI;
        patrolPoint = tankAI.PatrolPoints;
        index = 0;
        maxWaitTime = 1f;
    }

    public override NodeState Evaluate()
    {
        Debug.Log("Patrolling the area");
        //patrolPoint = null;
        if (patrolPoint == null)
        {
            state = NodeState.FAILURE;
            return state;
        }

        tankAI.SetAlertLevel(Color.yellow);
        int maxIndex = patrolPoint.Length;
        float distance = Vector3.Distance(patrolPoint[index].position, tankAI.transform.position);

        if (distance > 2f)
        {
            agent.isStopped = false;
            agent.SetDestination(patrolPoint[index].position);
            state = NodeState.RUNNING;
        }

        else
        {
            state = NodeState.SUCCESS;
            tankAI.IsWaitingDuringPatrol = true;

            if (tankAI.PatrolTime > maxWaitTime)
            {
                tankAI.IsWaitingDuringPatrol = false;
                tankAI.PatrolTime = 0f;
                index += 1;

                if (index == maxIndex)
                {
                    index = 0;
                }

                else if (index > maxIndex)
                {
                    index = maxIndex;
                }
            }
        }

        distance = Vector3.Distance(tankAI.Player.transform.position, tankAI.transform.position);

        if (distance <= tankAI.Radius)
        {
            state = NodeState.FAILURE;
        }

        return state;
    }
}
