﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCanonBall : MonoBehaviour
{
    private Rigidbody rb;

    [SerializeField]
    private float shootSpeed;

    [SerializeField]
    private float destroyAfterSeconds;

    [SerializeField]
    private float dmgValue;

    private TankAI tankAI;

    public TankAI TankAI { get => tankAI; set => tankAI = value;}

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {
        rb.AddForce(tankAI.transform.forward * shootSpeed, ForceMode.Force);
        Destroy(gameObject, destroyAfterSeconds);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            PlayerHealth playerHealth = collision.transform.GetComponent<PlayerHealth>();
            playerHealth.ReduceHealth(dmgValue);
            Destroy(gameObject);
        }
    }
}
