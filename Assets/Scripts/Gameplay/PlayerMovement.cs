﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed;

    [SerializeField]
    private float rotateSpeed;

    private void Update()
    {
        Move();
        Rotate();
    }

    private void Move()
    {
        Vector3 movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
        {
            movement = transform.forward * Time.deltaTime * moveSpeed;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            movement = (-transform.forward) * Time.deltaTime * moveSpeed;
        }

        transform.position += movement;
    }

    private void Rotate()
    {
        float xAxis = Input.GetAxis("Horizontal");
        Vector3 rotation = new Vector3(0f, xAxis, 0f)* Time.deltaTime * rotateSpeed;
        transform.eulerAngles += rotation;
    }
}
