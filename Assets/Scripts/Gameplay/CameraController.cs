﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private Vector3 moveOffset;

    [SerializeField]
    private Player player;

    void LateUpdate()
    {
        Move();
    }

    private void Move()
    {
        transform.position = player.transform.position + moveOffset;
    }
}
