﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    [SerializeField]
    private float rotateSpeed;

    [SerializeField]
    private Camera playerCamera;

    [SerializeField]
    private Transform playerTop;

    public Transform PlayerTop {get => playerTop;}

    void LateUpdate()
    {
        Rotate();
    }

    private void Rotate()
    {
        //rotate camera
        playerTop.Rotate(0f, Input.GetAxis("Mouse X") * rotateSpeed * Time.deltaTime, 0f, Space.World);
    }
}
